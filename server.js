const express = require('express')
const app = express()
const path = require('path')
const getData = require(path.join(__dirname + '/cronGetData.js'))
const sendData = require(path.join(__dirname + '/cronPutDataWp.js'))
const fs =require('fs')
const cron = require('node-cron');

let deleteFolderRecursive = function(path) {
    if( fs.existsSync(path) ) {
        fs.readdirSync(path).forEach(function(file,index){
            var curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

app.get('/', function (req, res) {

    res.sendFile(path.join(__dirname + '/client/index.html'));

})

app.get('/getData', function (req, res) {

    function DONE(){
        res.sendFile(path.join(__dirname + '/client/second.html'));
    }
    getData( req.query.USERNAME , req.query.PASSWORD , DONE )


})

app.get('/sendData', function (req, res) {

    let DataPagesRaw = require('./static/data');
    let roomsData = require('./static/roomsData.json')

    function DONE(){
        res.sendFile(path.join(__dirname + '/client/finish.html'));
    }
    sendData( req.query.DOMAIN , req.query.USERNAME , req.query.PASSWORD , DONE , DataPagesRaw , roomsData )

})

app.get('/finish', function (req, res) {

    let RemoveFiles = ['large','logo','rooms']

    RemoveFiles.map(function(item){
        let directory = path.join(__dirname + '/static/' + item );

        deleteFolderRecursive(path.join(__dirname + '/static/' + item));

        fs.mkdirSync(path.join(__dirname + '/static/' + item));

    })

    // fs.unlink(path.join(__dirname + '/static/data.json'))
    //
    // fs.unlink(path.join(__dirname + '/static/roomsData.json'))
    fs.stat(path.join(__dirname + '/static/roomsData.json'), function (err, stats) {
        console.log(stats);//here we got all information of file in stats variable

        if (err) {
            return console.error(err);
        }

        fs.unlink(path.join(__dirname + '/static/roomsData.json'),function(err){
            if(err) return console.log(err);
            console.log('file deleted successfully');
        });
    });


    fs.stat(path.join(__dirname + '/static/data.json'), function (err, stats) {
        console.log(stats);//here we got all information of file in stats variable

        if (err) {
            return console.error(err);
        }

        fs.unlink(path.join(__dirname + '/static/data.json'),function(err){
            if(err) return console.log(err);
            console.log('file deleted successfully');
        });
    });

    const shell = require('./child_helper');

    const commandList = [
        "pm2 reload server.js"
    ]

    shell.series(commandList , (err)=>{
        if(err){
            console.log(err)
        }
        console.log('cron run')
    });

    res.sendFile(path.join(__dirname + '/client/done.html'));

})

const server = app.listen(3000);
server.timeout = 0;