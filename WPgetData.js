const fs =require('fs');
let request = require('request');

//WP
let WPusername = "bill",
    WPpassword = "123",
    WPurl = "https://carbonaki.sitesdemo.com/api/wp-json/wp/v2/rooms",
    WPpages = "https://carbonaki.sitesdemo.com/api/wp-json/wp/v2/pages/",
    WPFieldsUrl = "https://carbonaki.sitesdemo.com/api/wp-json/acf/v3/rooms/",
    WPFieldsGalleries = "https://carbonaki.sitesdemo.com/api/wp-json/wp/v2/galleries/",
    WPImages = "https://carbonaki.sitesdemo.com/api/wp-json/wp/v2/media/",
    WPauth = "Basic " + new Buffer(WPusername + ":" + WPpassword).toString("base64");


function REQUESTpages(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
            console.log(response.statusCode)
            //Create
            if (!error && response.statusCode === 201) {

                //Gallery Media
                if( typeof body !== 'object'){

                    let info = JSON.parse(body)
                    let uroom =  url.formData.file[0].path.split('/')
                    WPHotelImagesId.push(info.id)

                }else{
                    //CREATE ROOM
                    console.log("kanamme create page")

                }

            }else{
                console.log(error)
            }

            //Update
            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){
                    let info = JSON.parse(body)

                    // console.log(body)

                    info.map(function(item){
                        console.log(item.title.rendered)
                        switch(item.title.rendered) {
                            case 'FACILITIES':

                                let GfacilitesArr = [];

                                if( item.acf.facilities.length >= 0 ){
                                    GfacilitesArr = item.acf.facilities.map(function(facilitie){
                                        return facilitie.facilitie
                                    })
                                }

                                let facilities = {
                                    "parallax_top_text": item.acf.parallax_top_text,
                                    "parallax_top_image": item.acf.parallax_top_image.url,
                                    "parallax_bottom_text": item.acf.parallax_bottom_text,
                                    "parallax_bottom_image": item.acf.parallax_bottom_image.url,
                                    "facilities": GfacilitesArr
                                        ,
                                    "id": item.id
                                }

                                fs.writeFile('./static/pages/facilities.json', JSON.stringify(facilities), 'utf8',(err)=> {
                                    console.log("change data output")
                                    return (err) ? console.log(err): console.log("The file pages was saved!");
                                });

                                break;
                            case 'GALLERY':

                                let GgallerieArr = [];

                                if( item.acf.gallery.length >= 0 ){
                                    GgallerieArr = item.acf.gallery.map(function(gallery){
                                        return gallery.url
                                    })
                                }

                                let gallery = {
                                    "parallax_top_text": item.acf.parallax_top_text,
                                    "parallax_top_image": item.acf.parallax_top_image.url,
                                    "parallax_bottom_text": item.acf.parallax_bottom_text,
                                    "parallax_bottom_image": item.acf.parallax_bottom_image.url,
                                    "gallery": GgallerieArr
                                }

                                fs.writeFile('./static/pages/gallery.json', JSON.stringify(gallery), 'utf8',(err)=> {
                                    console.log("change data output")
                                    return (err) ? console.log(err): console.log("The file pages was saved!");
                                });
                                break;
                            case 'HOMEPAGE':

                                let facilitiesArr = [];
                                let gallerieArr = [];

                                if( item.acf.facilities.length >= 0 ){
                                    facilitiesArr = item.acf.facilities.map(function(facilitie){
                                        return facilitie.facilitie
                                    })
                                }

                                if( item.acf.gallery.length >= 0 ){
                                    gallerieArr = item.acf.gallery.map(function(gallerie){
                                        return gallerie.url
                                    })
                                }

                                let homepage = {
                                    "parallax_top_text": item.acf.parallax_top_text,
                                    "parallax_top_image": item.acf.parallax_top_image.url,
                                    "parallax_bottom_text": item.acf.parallax_bottom_text,
                                    "parallax_bottom_image": item.acf.parallax_bottom_image.url,
                                    "facilities": facilitiesArr,
                                    "description": item.acf.description,
                                    "hero_image": item.acf.hero_image,
                                    "hero_text": item.acf.hero_text,
                                    "gallery": gallerieArr
                                }

                                fs.writeFile('./static/pages/homepage.json', JSON.stringify(homepage), 'utf8',(err)=> {
                                    console.log("change data output")
                                    return (err) ? console.log(err): console.log("The file pages was saved!");
                                })

                                break;
                            case 'LOCATION':
                                let location = {
                                    "parallax_top_text": item.acf.parallax_top_text,
                                    "parallax_top_image": item.acf.parallax_top_image.url,
                                    "parallax_bottom_text": item.acf.parallax_bottom_text,
                                    "parallax_bottom_image": item.acf.parallax_bottom_image.url,
                                    "directions": item.acf.directions,
                                    "lat": item.acf.lat,
                                    "long": item.acf.long
                                }

                                fs.writeFile('./static/pages/location.json', JSON.stringify(location), 'utf8',(err)=> {
                                    console.log("change data output")
                                    return (err) ? console.log(err): console.log("The file pages was saved!");
                                });
                                break;
                            case 'ACCOMMODATION':
                                let accommodation = {
                                    "parallax_top_text": item.acf.parallax_top_text,
                                    "parallax_top_image": item.acf.parallax_top_image.url,
                                    "parallax_bottom_text": item.acf.parallax_bottom_text,
                                    "parallax_bottom_image": item.acf.parallax_bottom_image.url,
                                    "description": item.acf.description,
                                    "hero_image": item.acf.hero_image,
                                    "hero_text": item.acf.hero_text
                                }

                                fs.writeFile('./static/pages/accommodation.json', JSON.stringify(accommodation), 'utf8',(err)=> {
                                    console.log("change data output")
                                    return (err) ? console.log(err): console.log("The file pages was saved!");
                                });
                                break;
                            case 'CONTACT':
                                let contact = {
                                    "parallax_top_text": item.acf.parallax_top_text,
                                    "parallax_top_image": item.acf.parallax_top_image.url,
                                    "parallax_bottom_text": item.acf.parallax_bottom_text,
                                    "parallax_bottom_image": item.acf.parallax_bottom_image.url
                                }

                                fs.writeFile('./static/pages/contact.json', JSON.stringify(contact), 'utf8',(err)=> {
                                    console.log("change data output")
                                    return (err) ? console.log(err): console.log("The file pages was saved!");
                                });
                                break;
                            default:
                                console.log("PAGE NOT FOUND")
                        }

                    })

                    fs.writeFile('./static/WPdataPages.json', JSON.stringify(body), 'utf8',(err)=> {
                        console.log("change data output")
                        return (err) ? console.log(err): console.log("The file pages was saved!");
                    });

                }else{
                    console.log("kaname update to " + body.id )
                }

            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}

function REQUESTrooms(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
            console.log(response.statusCode)
            //Create
            if (!error && response.statusCode === 201) {

                //Gallery Media
                if( typeof body !== 'object'){

                    let info = JSON.parse(body)
                    let uroom =  url.formData.file[0].path.split('/')
                    WPHotelImagesId.push(info.id)

                }else{
                    //CREATE ROOM
                    console.log("kanamme create page")

                }

            }else{
                console.log(error)
            }

            //Update
            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){
                    let info = JSON.parse(body)

                    let rooms = []

                    info.map(function(item){

                        let urlJson = './static/wprooms/'+ item.slug +'.json'
                        let facilities, more_rooms, gallery;

                        if( item.acf.facilities.length >= 0 ){
                            facilities = item.acf.facilities.map(function(facilities){
                                return facilities.facilitie
                            })
                        }

                        if( item.acf.more_rooms.length >= 0 ){
                            more_rooms = item.acf.more_rooms.map(function(room){
                                return room.select_room
                            })
                        }

                        if( item.acf.room_gallerie.length >= 0 ){
                            gallery = item.acf.room_gallerie.map(function(gallery){
                                return gallery.url
                            })
                        }

                        let page = {
                            "room_id": item.id,
                            "room_name" : item.title.rendered,
                            "room_image": item.acf.room_image.url,
                            "slug" : item.slug,
                            "gallery": gallery,
                            "room_description": item.acf.room_description,
                            "facilities_heading": item.acf.facilities_heading,
                            "facilities": facilities,
                            "book": item.acf.book,
                            "more_rooms": more_rooms
                        }

                        rooms.push(page)

                        fs.writeFile(urlJson, JSON.stringify(page), 'utf8',(err)=> {
                            console.log("change data output")
                            return (err) ? console.log(err): console.log("The file pages was saved!")
                        });

                    })

                    fs.writeFile('./static/WPdataRooms.json', JSON.stringify(rooms), 'utf8',(err)=> {
                        console.log("change data output")
                        return (err) ? console.log(err): console.log("The file pages was saved!");
                    });

                }else{
                    console.log("kaname update to " + body.id )
                }

            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}


function REQUESTgallerie(url) {

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
            console.log(response.statusCode)
            //Update
            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){

                    let info = JSON.parse(body)

                    let galleries = []


                    info.map(function(item){

                        let urlJson = './static/wpgalleries/'+ item.slug +'.json'
                        let  gallerie;


                        if( item.acf.gallery.length >= 0 ){
                            gallerie = item.acf.gallery.map(function(gallerie){
                                return gallerie.url
                            })
                        }

                        let page = {
                            "post_id": item.id,
                            "post_name": item.title.rendered,
                            "post_slug": item.slug,
                            "post_image": item.acf.gallery_image,
                            "gallery": gallerie,
                            "show_front": item.acf.show_in_front,
                        }

                        galleries.push(page)


                        fs.writeFile(urlJson, JSON.stringify(page), 'utf8',(err)=> {
                            console.log("change data output")
                            return (err) ? console.log(err): console.log("The file pages was saved!")
                        });

                    })

                    fs.writeFile('./static/WPdataGalleries.json', JSON.stringify(galleries), 'utf8',(err)=> {
                        console.log("change data output")
                        return (err) ? console.log(err): console.log("The file pages was saved!");
                    });

                }else{
                    console.log("kaname update to " + body.id )
                }

            }else{
                console.log(error)
            }

            resolve("kaname Create ksana" + response.statusCode)
        });
    });
}



async function putDataToPages() {

    const WPGalleries = {
        url : WPFieldsGalleries,
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
        },
        method: "GET"
    }


    const Wgalleries = await REQUESTgallerie(WPGalleries)
    console.log('SHOULD WORK: Galleries');


    const WPRooms = {
        url : WPurl,
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
        },
        method: "GET",
        // body: JsonBody,
        //json: true
    }

    const Wrooms = await REQUESTrooms(WPRooms);

    console.log('SHOULD WORK: CREATE ROOMS');

    const WPPAGES = {
        url : WPpages,
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
        },
        method: "GET",
        // body: JsonBody,
        //json: true
    }


    const Wpages = await REQUESTpages(WPPAGES)
    console.log('SHOULD WORK: CREATE PAGES');





}

putDataToPages()
