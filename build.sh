#!/usr/bin/env bash

if [[ $1 == "get" ]];
	then
	    mkdir static
	    mkdir static/large
	    mkdir static/rooms
	    mkdir static/logo
	    cd static && touch data.json
	    echo "file json created"
	    cd ../

        export WP_SOURCE_USERNAME=$2
        export WP_SOURCE_PASSWORD=$3

        node cronGetData.js

	echo "$1 created run yarn dev"

elif [[ $1 == "end" ]];
	then
	rm -rf static/large
	rm -rf static/logo
	rm -rf static/rooms
	rm -rf static/data.json
	rm -rf static/roomsData.json

echo "$1 environment created"

elif [[ $1 == "push" ]];
	then
    export WP_DESTINATION=$2
    export WP_DESTINATION_USERNAME=$3
    export WP_DESTINATION_PASSWORD=$4
    node cronPutDataWp.js

    echo "$1 environment created"

else
echo "Parameter not right."
fi

