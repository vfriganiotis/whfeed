

// Property Code: MALTEZA
// Username: MALTEZA
// Password: 705567A438EC8B36FE6383839A866F390704CCFC
//
// username = "ATATRIUM",
//    password = "42F8A83C68009550CDCD4BCAA87C45B55E8E7D01",
//    url = "https://rest.reserve-online.net/property/ATATRIUM",
//
// username = "TERRAMALT"
// pass = 7C79962B6147FCB22B1EA1309E65BCCB5D90625D

//Property Code: CARBONAKI
//Username: CARBONAKI
//Password: 390CCDA87D241453BC5E815A487DFD500E5F7936
//

let GETALLDATAFROMAPI = function(USERNAME,PASSWORD,ENDCALL){

    const fs =require('fs');

    let request = require('request'),
        username = USERNAME,
        password = PASSWORD,
        url = "https://rest.reserve-online.net/property/" + username,
        offersUrl = "https://rest.reserve-online.net/offers/" + username,
        roomUrl = "https://rest.reserve-online.net/room/" + username,
        auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

    console.log(username)

    const download = function(uri, filename, callback){
        request.head(uri, function(err, res, body){
            request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
        });
    };

    function REQUEST_ROOMS_DATA(url) {

        return new Promise((resolve, reject) => {
            request(url, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    const content = JSON.stringify(body);
                    let info = JSON.parse(body)
                    let strRooms = content;

                    info.data.rooms.map(function(room){

                        let dir = './static/rooms/' + room.name;

                        if (!fs.existsSync(dir)){
                            fs.mkdirSync(dir);
                        }

                        const code = username.toLocaleLowerCase() + '-' + room.code.toLocaleLowerCase();

                        let regex = new RegExp( 'https:\/\/cdn.webhotelier.net\/photos\/w=1920\/' + code , "g");
                        const largePhotos = regex;
                        const roomPath = "../static/rooms/" + room.name;

                        strRooms = strRooms.replace( largePhotos , roomPath )
                        room.photos.map(function(item){

                            let split = item.large.split('/')
                            let url = './static/rooms/' + room.name + '/' + split[split.length-1];

                            download(item.large, url, function(){
                                //console.log('done');
                            });

                            return item

                        })

                        return room
                    })

                    fs.writeFile('./static/roomsData.json', strRooms, 'utf8',(err)=> {
                        console.log("change data output")
                        return (err) ? console.log(err): console.log("The file pages was saved!");
                    });

                }else{
                    console.log(error)
                }

                resolve("kaname create to JSON " + response.statusCode)
            })
        })

    }

    function REQUESTDATA(url) {

        return new Promise((resolve, reject) => {
            request( url , function(error, response, body){

                if (!error && response.statusCode === 200) {

                    const content = JSON.stringify(body);
                    let info = JSON.parse(body);
                    info.data.photos.map(function(item){

                        let split = item.large.split('/')
                        let url = './static/large/' + split[split.length-1];

                        download(item.large, url, function(){
                            console.log('done');
                        });

                        return item
                    })

                    let logo = info.data.logourl.split('/')
                    let logoUrls = './static/logo/' + logo[logo.length-1];

                    if(!(info.data.logourl === '' || info.data.logourl === 'null')){
                        download(info.data.logourl , logoUrls, function(){
                            console.log('logo done');
                        });
                    }

                    const code = info.data.code.toLowerCase();
                    let regex = new RegExp( 'https:\/\/cdn.webhotelier.net\/photos\/w=1920\/' + code , "g");
                    const largePhotos = regex;

                    const logoUrl = /https:\/\/cdn.webhotelier.net\/photos\/h=(\d+):v=(\d+)\/templates\/(\d+)\//g;

                    const str = content.replace( largePhotos , "../static/large")
                        .replace( logoUrl , "../static/logo/")

                    fs.writeFile('./static/data.json', str, 'utf8',(err)=> {
                        console.log("change data output")
                        return (err) ? console.log(err): console.log("The file pages was saved!");
                    });

                    console.log(3)

                } else {
                    console.log(error)
                }

                resolve("kaname create to JSON " + response.statusCode)

            });
        });
    }

    function REQUESTOFFERS(url) {

        return new Promise((resolve, reject) => {
            request( url , function(error, response, body){

                if (!error && response.statusCode === 200) {
                    const content = JSON.stringify(body);
                    let info = JSON.parse(body);
                    console.log(info)
                    // info.data.photos.map(function(item){
                    //
                    //     let split = item.large.split('/')
                    //     let url = './static/large/' + split[split.length-1];
                    //
                    //     download(item.large, url, function(){
                    //         console.log('done');
                    //     });
                    //
                    //     return item
                    // })
                    //
                    // let logo = info.data.logourl.split('/')
                    // let logoUrls = './static/logo/' + logo[logo.length-1];
                    //
                    // download(info.data.logourl , logoUrls, function(){
                    //     console.log('logo done');
                    // });
                    // const code = info.data.code.toLowerCase();
                    // let regex = new RegExp( 'https:\/\/cdn.webhotelier.net\/photos\/w=1920\/' + code , "g");
                    // const largePhotos = regex;
                    //
                    // const logoUrl = /https:\/\/cdn.webhotelier.net\/photos\/h=(\d+):v=(\d+)\/templates\/(\d+)\//g;
                    //
                    // const str = content.replace( largePhotos , "../static/large")
                    //     .replace( logoUrl , "../static/logo/")
                    //
                    // fs.writeFile('./static/data.json', str, 'utf8',(err)=> {
                    //     console.log("change data output")
                    //     return (err) ? console.log(err): console.log("The file pages was saved!");
                    // });

                } else {
                    console.log(error)
                }

                resolve("kaname create to JSON " + response.statusCode)

            });
        });
    }

    async function getDataFromApi(){

        console.log("DOWNLOAD HOTEL IMGS START")
        let WPMediaPost =  {
            url : url,
            headers : {
                "Authorization" : auth,
                "Content-Type" : "application/json",
                "Access-Control-Allow-Origin": "*",
                "Accept": "application/json"
            }
        };

        const v = await REQUESTDATA(WPMediaPost)

        console.log("DOWNLOAD HOTEL IMGS DONE")
        console.log("DOWNLOAD ROOMS IMGS")

        let WPROOMSPOSTS = {
            url : roomUrl,
            headers : {
                "Authorization" : auth,
                "Content-Type" : "application/json",
                "Access-Control-Allow-Origin": "*",
                "Accept": "application/json"
            }
        }

        const v2 = await REQUEST_ROOMS_DATA(WPROOMSPOSTS)
        console.log("DOWNLOAD ROOMS IMGS DONE")

        let WPOFFERS = {
            url : offersUrl,
            headers : {
                "Authorization" : auth,
                "Content-Type" : "application/json",
                "Access-Control-Allow-Origin": "*",
                "Accept": "application/json"
            }
        }

        const v3 = await REQUESTOFFERS(WPOFFERS)
        console.log("DOWNLOAD OFFERS DONE")

        ENDCALL()
        console.log("teleiwsame to prwto stadio")
    }

    getDataFromApi()
}


module.exports = GETALLDATAFROMAPI

