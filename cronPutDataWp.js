
let SENDALLDATAFROMAPI = function(DOMAIN,USERNAME,PASSWORD,ENDCALL,DataPagesRaw,roomsData){

    const fs =require('fs');
    let request = require('request');

//WP
    let WPusername = USERNAME,
        WPpassword = PASSWORD,
        WPurl = "https://" + DOMAIN + "/wp-json/wp/v2/rooms",
        WPpages = "https://" + DOMAIN + "/wp-json/wp/v2/pages/",
        WPservices = "https://" + DOMAIN + "/wp-json/wp/v2/services",
        WPurlGallery = "https://" + DOMAIN + "/wp-json/wp/v2/galleries",
        WPFieldsOptions = "https://" + DOMAIN + "/wp-json/acf/v3/options/contact-settingsb",
        WPImages = "https://" +  DOMAIN + "/wp-json/wp/v2/media/",
        WPoptionPage = "https://" +  DOMAIN + "/wp-json/wp/v2/hotel_info/",
        WPauth = "Basic " + new Buffer(WPusername + ":" + WPpassword).toString("base64");

    const DataPages = JSON.parse(DataPagesRaw);

    let HotelFacilities = []
    let ServicesFacilities = "";

    let thisExpressions = [
        /parking/,/doctor/,/fax/,/jacuzzi/,/transfer/,/cleaning/,/safe/ ,/spa/,
        /safebox/,/balcony/,/coffee/,/mattress/,/coco/,/conditioning/,/breakfast/, /bar/, /24h/ ,/condition/,
        /sunbeds/ , /internet/ , /wiffi/ , /business/ , /laundry/ , /sunbeds/ , /dimensions/, /wi-fi/ ,
        /tv/ , /refrigerator/ , /concierge/ , /massage/ , /baby/ , /rental/ , /swimming/ , /pets/ , /reception/ , /sauna/, /gym/,
        /bathrobe/, /hammam/,/sports/,/shower/,/disabled/,/restaurant/, /luggage/ , /room/ , /tennis/ , /desk/ , /tours/ ,
        /library/ , /hairdresser/ , /makeup/ , /hairdryer/
    ];

    function matchInArray(string, expressions) {

        let len = expressions.length,
            i = 0;

        for (; i < len; i++) {
            if (string.match(expressions[i])) {
                return true;
            }
        }

        return false;

    };

    DataPages.data.facilities.map(function(item){

        HotelFacilities.push({"facilitie": item})

        let IconsMath = item.split(' ');
        let findMatchWord = false;
        IconsMath.map(function(word){
            if( matchInArray(word.toLocaleLowerCase(), thisExpressions) && !findMatchWord ){
                ServicesFacilities += '<li>' + item +  '<img src="https://greece-hotel.info/icons/' + word.toLocaleLowerCase() + '.png"></li>'
                findMatchWord = true;
            }
        })

        if(!findMatchWord){
            ServicesFacilities += "<li>" + item + "</li>"
        }

    })

    console.log(ServicesFacilities)


    let OptionsPages = [
        {
            "name": "HEADER",
            "data": [
                {
                    "logo": ""
                },
                {
                    "telephone": DataPages.data.name
                },
                {
                    "book_now": "ssasaasas"
                },
                {
                    "mobile_logo" : ""
                }
            ]
        },
        {
            "name": "FOOTER",
            "data": [
                {
                    "hotel_name_and_development": ""
                },
                {
                    "mhte": ""
                },
                {
                    "instagram": ""
                },
                {
                    "facebook" : ""
                },
                {
                    "twitter" : ""
                }
            ]
        },
        {
            "name": "BUSINESS",
            "data": [
                {
                    "name": ""
                },
                {
                    "streetaddress": ""
                },
                {
                    "addresslocality": ""
                },
                {
                    "postalcode" : ""
                },
                {
                    "telephone" : ""
                },
                {
                    "openinghours": ""
                },
                {
                    "latitude": ""
                },
                {
                    "longitude": ""
                },
                {
                    "url" : ""
                },
                {
                    "logo" : ""
                },
                {
                    "image": ""
                },
                {
                    "pricerange": ""
                },
                {
                    "ratingvalue" : ""
                },
                {
                    "ratingcount" : ""
                }
            ]
        }
    ]


    let pages = [
        {
            "name": "HOMEPAGE",
            "data": [
                {
                    "hero_image": ""
                },
                {
                    "hero_text": DataPages.data.name
                },
                {
                    "facilities": HotelFacilities
                },
                {
                    "description" : ""
                }
            ]
        },
        {
            "name": "SERVICES",
            "data": []
        },
        {
            "name": "ACCOMMODATION",
            "data": []
        },
        {
            "name": "GALLERY",
            "data": []
        },
        {
            "name": "LOCATION",
            "data": []
        },
        {
            "name": "CONTACT",
            "data": []
        }
    ];

    let WP_PAGES = [

    ]

    const hotelImgs = './static/large/';
    let hotelIMAGES = [];
    let WPHotelImagesId = [];

    let files = fs.readdirSync(hotelImgs);

    files.forEach(function(file) {
        hotelIMAGES.push( hotelImgs + file )
    })

    async function putDataToPages() {

        for ( const Litem of hotelIMAGES){

            let formData = {
                file: [fs.createReadStream(Litem)],
            };

            let WPMediaPost = {
                url : WPImages,
                headers : {
                    "Authorization" : WPauth,
                    "Content-Type" : "application/x-www-form-urlencoded",
                    "Access-Control-Allow-Origin": "*",
                    "Accept": "application/json",
                    "Content-Disposition" : "attachment;"
                },
                method: "POST",
                formData: formData
            };

            const html = await REQUESTpages(WPMediaPost);
        }

        const LogoImg = './static/logo/';

        let LogoImgfiles = fs.readdirSync(LogoImg);

        if( !(LogoImgfiles.length === 0)){


            console.log(LogoImgfiles)
            //logo
            let formData = {
                file: [fs.createReadStream("./static/logo/" + LogoImgfiles[0])],
            };

            // console.log(111)
            // console.log(formData)

            let WPLogo = {
                url : WPImages,
                headers : {
                    "Authorization" : WPauth,
                    "Content-Type" : "image/png",
                    "Access-Control-Allow-Origin": "*",
                    "Content-Disposition" : "attachment; filename=" + LogoImgfiles[0],
                    "cache-control": "no-cache"

                },
                method: "POST",
                formData: formData
            };

            const htmlc = await REQUESTpages(WPLogo);
        }

        console.log("proxwrisame GIA TA options")
        const WPCreateOptionsPage = {
            url : WPoptionPage,
            headers : {
                "Authorization" : WPauth,
                "Content-Type" : "application/json",
                "Access-Control-Allow-Origin": "*",
                "Accept": "application/json"
            },
            method: "GET",
            // body: JsonBody,
            //json: true
        }

        const voptions = await REQUESTpages(WPCreateOptionsPage)

        for( const ImItem of WP_PAGES ) {
            for (const item of OptionsPages) {
                if (ImItem.name === item.name) {
                    let WPpagesSingle = WPoptionPage + ImItem.id

                    let UpdateFields = {
                        "fields": {
                            "theme": "theme1",
                            "book_now": DataPages.data.bookurl,
                            "telephone": DataPages.data.contact.tel,
                            "logo": WPHotelImagesId[WPHotelImagesId.length - 1],
                            "mobile_logo": WPHotelImagesId[WPHotelImagesId.length - 1],
                            "hotel_name_and_development": DataPages.data.name,
                            "mhte": "25682569853",
                            "instagram": "#",
                            "facebook": "#",
                            "twitter": "#",
                            "name": DataPages.data.name,
                            "streetaddress": DataPages.data.location.address,
                            "addresslocality": DataPages.data.location.name,
                            "postalcode" : DataPages.data.location.zip,
                            "openinghours": "24h",
                            "latitude": DataPages.data.location.lat,
                            "longitude": DataPages.data.location.lon,
                            "url" : "",
                            "image": WPHotelImagesId[0],
                            "pricerange": "$-$",
                            "ratingvalue" : "5",
                            "ratingcount" : "40"
                        }
                    }

                    const WPUpdatePost = {
                        url: WPpagesSingle,
                        headers: {
                            "Authorization": WPauth,
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Origin": "*",
                            "Accept": "application/json"
                        },
                        method: "POST",
                        body: UpdateFields,
                        json: true
                    }

                    console.log('asaswwwwqqqqqqqqqqqqqqqqqqqqq')
                    console.log(WPpagesSingle)
                    console.log(WPHotelImagesId)
                    const vss = await REQUESTpages(WPUpdatePost)
                }
            }
        }

        console.log("proxwrisame")
        const WPCreatePost = {
            url : WPpages,
            headers : {
                "Authorization" : WPauth,
                "Content-Type" : "application/json",
                "Access-Control-Allow-Origin": "*",
                "Accept": "application/json"
            },
            method: "GET",
            // body: JsonBody,
            //json: true
        }

        const v = await REQUESTpages(WPCreatePost)

        console.log('asqqqqqqqqqqqqqqq')
        console.log(WP_PAGES)


        console.log('SHOULD WORK: READ Post');
        console.log("proxwrisame2")
        for( const ImItem of WP_PAGES ){
            for( const item of pages){
                if( ImItem.name === item.name) {
                    let WPpagesSingle = WPpages + ImItem.id

                    let UpdateFields = {
                        "fields": {
                            "main_slider_heading": "Welcome To",
                            "main_slider_content": DataPages.data.name,
                            "main_slider_images":[
                                {
                                    image: WPHotelImagesId[0],
                                    text_top: DataPages.data.name + ' Welcomes You',
                                    sub_text: "a paradise in earth"
                                },
                            ],
                            "worthseeings_head": "LOCATION",
                            "worthseeings_sub_head": "A fantastic environment that satisfies all your wishes",
                            "worthseeings_image": WPHotelImagesId[1],
                            "hero_image": WPHotelImagesId[1],
                            "hero_text": DataPages.data.name,
                            "room_description": "",
                            "background_Hero": WPHotelImagesId[0],
                            "facilities_heading":"FACILITIES",
                            "facilities": HotelFacilities,
                            "description": DataPages.data.description,
                            "parallax_top_image": WPHotelImagesId[0],
                            "parallax_top_text" : DataPages.data.name,
                            "parallax_bottom_image": WPHotelImagesId[1],
                            "parallax_bottom_text" : DataPages.data.name,
                            "gallery" : WPHotelImagesId,
                            "lat": DataPages.data.location.lat,
                            "long": DataPages.data.location.lon,
                            "directions": DataPages.data.directions,
                            "google_map_lat":DataPages.data.location.lat,
                            "google_map_lng":DataPages.data.location.lon,
                            "location": DataPages.data.location.name + ", " + DataPages.data.location.address + ", zip: " + DataPages.data.location.zip,
                            "overview": DataPages.data.name,
                            "overview_content": DataPages.data.description,
                            "loverview": "Ideal location",
                            "loverview_content": DataPages.data.directions,
                            "why_choose_us": "Why Choose Us",
                            "why_choose_us_content":[
                                {
                                    text: "Feel-like-home experience"

                                },
                                {
                                    text: "Ideally situated"
                                },
                                {
                                    text: "Personalized services and treatments"
                                }
                            ],
                            "book_now": DataPages.data.bookurl,
                            "telephone":[
                                {
                                    "phone_number": DataPages.data.contact.tel
                                }
                            ],
                            "overview_text": DataPages.data.name,
                            "fax_number":DataPages.data.contact.fax,
                            "mail": DataPages.data.contact.email,
                            "overview_background_color" : '#616161'
                        }
                    }

                    const WPUpdatePost = {
                        url : WPpagesSingle,
                        headers : {
                            "Authorization" : WPauth,
                            "Content-Type" : "application/json",
                            "Access-Control-Allow-Origin": "*",
                            "Accept": "application/json"
                        },
                        method: "POST",
                        body: UpdateFields,
                        json: true
                    }

                   const v = await REQUESTpages(WPUpdatePost)
                }
            }
        }

    }
    putDataToPages()

    function REQUESTpages(url) {

        return new Promise((resolve, reject) => {

            request( url , function(error, response, body){

                console.log(response.statusCode)
                //Create
                if (!error && response.statusCode === 201) {

                    //Gallery Media
                    if( typeof body !== 'object'){

                        let info = JSON.parse(body)

                        // let uroom =  url.formData.file[0].path.split('/')

                        WPHotelImagesId.push(info.id)

                    }else{
                        //CREATE ROOM
                        console.log("kanamme create page")

                    }

                }else{
                    console.log(error)
                }

                //Update
                if (!error && response.statusCode === 200) {

                    if( typeof body === 'string'){
                        let info = JSON.parse(body)
                        console.log("Eidame ta pages")

                        info.map(function(item){
                            WP_PAGES.push({"name":item.title.rendered ,"id":item.id})
                        })
                    }else{
                        console.log("kaname update to " + body.id )
                    }

                }else{
                    console.log(error)
                }

                resolve("kaname Create ksana" + response.statusCode)
            });
        });
    }

//CREATE POST


    let info = JSON.parse(roomsData)

    const RoomImgs = './static/rooms/';
    let roomsIMAGES = [];

    let filesRooms = fs.readdirSync(RoomImgs);

    filesRooms.forEach(function(file) {

        let RoomsName =  fs.readdirSync(RoomImgs + file)
        roomsIMAGES.push({name: file , images : []})
        RoomsName.forEach(function(fileImg) {

            fileImg = RoomImgs + file + '/'+ fileImg
            roomsIMAGES.map(function(item,i){

                if( item.name === file ){
                    roomsIMAGES[i].images.push( fileImg )
                }

            })

        })

    })

    info.data.rooms.map(function(item,i){
        filesRooms.map(function(res){
            if( item.name === res){
                roomsIMAGES[i].code = item.code
                roomsIMAGES[i].book = DataPages.data.bookurl + "?nights=1&room=" + item.code
            }
        })

    })

    function REQUESTServices(url,ss) {

        return new Promise((resolve, reject) => {
            request( url , function(error, response, body){

                //Create
                if (!error && response.statusCode === 201) {

                    //Gallery Media
                    if( typeof body !== 'object'){

                        let info = JSON.parse(body)
                        let uroom =  url.formData.file[0].path.split('/')

                        for(let i = 0; i < roomsIMAGES.length; i++){
                            if( roomsIMAGES[i].name === uroom[uroom.length - 2]){

                                for(let x =0; x < roomsIMAGES[i].images.length; x++){
                                    if( roomsIMAGES[i].images[x] === url.formData.file[0].path){

                                        roomsIMAGES[i].images[x] = info.id

                                    }
                                }

                            }
                        }

                    }else{
                        //CREATE ROOM
                        for(let i = 0; i < roomsIMAGES.length; i++){
                            if( roomsIMAGES[i].name === body.title.raw){
                                roomsIMAGES[i].roomName = body.id
                            }
                        }

                    }

                }else{
                    console.log(error)
                }

                //Update
                if (!error && response.statusCode === 200) {
                    console.log(2)
                }else{
                    console.log(error)
                }


                resolve("kaname Create ksana" + response.statusCode)
            });
        });
    }

    function REQUEST(url,ss) {

        return new Promise((resolve, reject) => {
            request( url , function(error, response, body){

                //Create
                if (!error && response.statusCode === 201) {

                    //Gallery Media
                    if( typeof body !== 'object'){

                        let info = JSON.parse(body)
                        let uroom =  url.formData.file[0].path.split('/')

                        for(let i = 0; i < roomsIMAGES.length; i++){
                            if( roomsIMAGES[i].name === uroom[uroom.length - 2]){

                                for(let x =0; x < roomsIMAGES[i].images.length; x++){
                                    if( roomsIMAGES[i].images[x] === url.formData.file[0].path){

                                        roomsIMAGES[i].images[x] = info.id

                                    }
                                }

                            }
                        }

                    }else{
                        //CREATE ROOM
                        for(let i = 0; i < roomsIMAGES.length; i++){
                            if( roomsIMAGES[i].name === body.title.raw){
                                roomsIMAGES[i].roomName = body.id
                            }
                        }

                    }

                }else{
                    console.log(error)
                }

                //Update
                if (!error && response.statusCode === 200) {
                    console.log(2)
                }else{
                    console.log(error)
                }


                resolve("kaname Create ksana" + response.statusCode)
            });
        });
    }

    let ameData = [

        {
            icon: "./static/amenities/icc1.png",
            name: "icc1",
            wpId: 1
        },
        {
            icon: "./static/amenities/icc2.png",
            name: "icc2",
            wpId: 2
        },
        {
            icon: "./static/amenities/icc3.png",
            name: "icc3",
            wpId: 3
        },
        {
            icon: "./static/amenities/icc4.png",
            name: "icc4",
            wpId: 4
        }


    ]

    let vv = 0;
    function REQUESTAme(url) {

        return new Promise((resolve, reject) => {
            request( url , function(error, response, body){

                //Create
                if (!error && response.statusCode === 201) {

                    //Gallery Media
                    if( typeof body !== 'object'){

                        let info = JSON.parse(body)


                        ameData[vv].id = info.id

                        vv++;
                    }

                }else{
                    console.log(error)
                }

                resolve("kaname Create ksana" + response.statusCode)
            });
        });
    }



//
//
    async function myBackEndLogic() {

        try {
            // FOR EACH LOOP ASYNC/AWAIT


            for( let i = 0; i < ameData.length; i++){

                let formData = {
                    file: [fs.createReadStream(ameData[i].icon)],
                };

                let WPMediaPost = {
                    url : WPImages,
                    headers : {
                        "Authorization" : WPauth,
                        "Content-Type" : "application/x-www-form-urlencoded",
                        "Access-Control-Allow-Origin": "*",
                        "Accept": "application/json",
                        "Content-Disposition" : "attachment;"
                    },
                    method: "POST",
                    formData: formData
                };

                const html = await REQUESTAme(WPMediaPost);
            }

            for( let i = 0; i < roomsIMAGES.length; i++){

                for ( const Litem of roomsIMAGES[i].images){

                    let formData = {
                        file: [fs.createReadStream(Litem)],
                    };

                    let WPMediaPost = {
                        url : WPImages,
                        headers : {
                            "Authorization" : WPauth,
                            "Content-Type" : "application/x-www-form-urlencoded",
                            "Access-Control-Allow-Origin": "*",
                            "Accept": "application/json",
                            "Content-Disposition" : "attachment;"
                        },
                        method: "POST",
                        formData: formData
                    };

                    const html = await REQUEST(WPMediaPost,WPMediaPost.url);
                }
            }

            for (const item of info.data.rooms) {

                const JsonBody = {
                    "title" : "Jedi Rooms",
                    "status" : "publish"
                }

                JsonBody.title = item.name

                const WPCreatePost = {
                    url : WPurl,
                    headers : {
                        "Authorization" : WPauth,
                        "Content-Type" : "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Accept": "application/json"
                    },
                    method: "POST",
                    body: JsonBody,
                    json: true
                }


                const v = await REQUEST(WPCreatePost , WPCreatePost.url)
                console.log('SHOULD WORK: 3 kaname CREATE Post');


            }

            for( let x = 0; x < roomsIMAGES.length; x++){

                let WPFieldsUrl = "https://" + DOMAIN + "/wp-json/acf/v3/rooms/" + roomsIMAGES[x].roomName;

                let UpdateFields = {
                    "fields": {
                        "main_slider_images":[
                            //222
                        ],
                        "overview": "",
                        "facilities_heading":"",
                        "facilities": [],
                        "up_to": 1,
                        "book_now" : "",
                        "featured_image": ""
                    }
                }

                roomsIMAGES[x].images.map(function(item){
                    UpdateFields.fields.main_slider_images.push(item)
                })

                UpdateFields.fields.featured_image = roomsIMAGES[x].images[0]
                // UpdateFields.fields.featured_image.id = roomsIMAGES[x].images[0]

                info.data.rooms.map(function(item){
                    if( item.name === roomsIMAGES[x].name){
                        UpdateFields.fields.overview = item.description;
                        UpdateFields.fields.up_to = item.capacity.max_pers;
                        UpdateFields.fields.book_now = item.code;
                        UpdateFields.fields.facilities_heading = "SERVICES & AMENITIES";
                        //'<li>Max. Capacity: up to ' + item.capacity.max_pers + ' guests</li><li>Location: Ground floor (one step down)</li><li> Exterior: Patio (shared or private)</li><li>View: No view</li>';
                        UpdateFields.fields.excerpt = item.description;
                        item.amenities.map(function(amenitie){
                            UpdateFields.fields.facilities += "<li>" + amenitie + "</li>"
                        })

                        UpdateFields.fields.hightligts = [
                            {"icon-hightligt": ameData[0].id,"text-hightligt": "up to " + item.capacity.max_pers  + " guest"},
                            {"icon-hightligt": ameData[1].id,"text-hightligt": "28 sq.m"},
                            {"icon-hightligt": ameData[2].id,"text-hightligt": "2 Twin beds or 1 Double bed"},
                            {"icon-hightligt": ameData[3].id,"text-hightligt": "free WiFi"},
                        ]
                    }
                })


                let WPUpdatePost = {
                    url : WPFieldsUrl,
                    headers : {
                        "Authorization" : WPauth,
                        "Content-Type" : "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Accept": "application/json"
                    },
                    method: "POST",
                    body: UpdateFields,
                    json: true
                }

                const cc = await REQUEST(WPUpdatePost,WPUpdatePost.url)
                console.log('SHOULD WORK: 4 kaname update Post');
            }

            for (const item of info.data.rooms) {

                const JsonBody = {
                    "title" : "Jedi Rooms",
                    "status" : "publish"
                }

                JsonBody.title = item.name

                const WPCreatePost = {
                    url : WPurlGallery,
                    headers : {
                        "Authorization" : WPauth,
                        "Content-Type" : "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Accept": "application/json"
                    },
                    method: "POST",
                    body: JsonBody,
                    json: true
                }

                const v = await REQUEST(WPCreatePost,WPCreatePost.url)
                console.log('SHOULD WORK: 3 kaname CREATE Gallery');

            }

            for( let x = 0; x < roomsIMAGES.length; x++){

                let WPFieldsUrl = "https://" + DOMAIN + "/wp-json/acf/v3/galleries/" + roomsIMAGES[x].roomName;

                let UpdateFields = {
                    "fields": {
                        "gallery":[
                            //222
                        ],
                        "gallery_image" : ""
                    }
                }

                roomsIMAGES[x].images.map(function(item){
                    UpdateFields.fields.gallery.push(item)
                })

                UpdateFields.fields.gallery_image =  roomsIMAGES[x].images[0]


                let WPUpdatePost = {
                    url :WPFieldsUrl,
                    headers : {
                        "Authorization" : WPauth,
                        "Content-Type" : "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Accept": "application/json"
                    },
                    method: "POST",
                    body: UpdateFields,
                    json: true
                }

                const cc = await REQUEST(WPUpdatePost,WPUpdatePost.url )
                console.log('SHOULD WORK: 4 kaname update Post');
            }

            const JsonBodyServices = {
                "title" : "Jedi Rooms",
                "status" : "publish",
                "content": "The facilities of all refurbished and comfortable guestrooms of our hotel are as follows: " + ServicesFacilities,
                "excerpt": ServicesFacilities,
                "featured_media": roomsIMAGES[0].images[0]
            }

            JsonBodyServices.title = 'Services'

            const WPCreatePostS = {
                url : WPservices,
                headers : {
                    "Authorization" : WPauth,
                    "Content-Type" : "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Accept": "application/json"
                },
                method: "POST",
                body: JsonBodyServices,
                json: true
            }

            const v = await REQUESTServices(WPCreatePostS,WPCreatePostS.url )
            console.log('SHOULD WORK: 3 kaname CREATE Gallery');

            console.log("TELOSSSSSSSSSSSSSSSS")




            let JsonBodyGal = {
                "title" : "HOTEL",
                "status" : "publish",
                "fields": {
                    "gallery": WPHotelImagesId,
                    "gallery_image" : ""
                }
            }


            JsonBodyGal.fields.gallery_image =  WPHotelImagesId[0]

            const WPCreatePostG = {
                url : WPurlGallery,
                headers : {
                    "Authorization" : WPauth,
                    "Content-Type" : "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Accept": "application/json"
                },
                method: "POST",
                body: JsonBodyGal,
                json: true
            }

            const vG = await REQUEST(WPCreatePostG, WPCreatePostG.url )


            console.log("assasasa PAGES")


            ENDCALL()


            console.log(WP_PAGES)

        } catch (error) {

            console.error('ERROR:');
            console.error(error);

        }

    }

//run async function
    myBackEndLogic();

    let OptionsPage = {

        "fields": {
            // "google_map_lat": "",
            "mail" : "fdsfdsfdsf"
            //"gallery_image" : ""
        }

    }


// OptionsPage.telephone = [{"phone_number":DataPages.data.contact.tel}]
// OptionsPage.fax_number = DataPages.data.contact.fax
// OptionsPage.google_map_lat = DataPages.data.location.lat
// OptionsPage.google_map_long = DataPages.data.location.lon
// OptionsPage.mail = DataPages.data.location.email

    async function putDataOptions() {

        const WPCreatePost = {
            url : WPFieldsOptions,
            headers : {
                "Authorization" : WPauth,
                "Content-Type" : "application/json",
                "Access-Control-Allow-Origin": "*",
                "Accept": "application/json"
            },
            method: "POST",
            body: OptionsPage,
            json: true
        }


        const v = await REQUEST(WPCreatePost,WPCreatePost.url )
        console.log('SHOULD WORK: READ Post');


    }

    putDataOptions()

}


module.exports = SENDALLDATAFROMAPI



